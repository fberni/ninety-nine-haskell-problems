-- Problem 25
-- λ> rnd_permu "abcdef"
-- "badcef"

module P25 where

import Control.Monad
import System.Random

rnd_perm :: [a] -> IO [a]
rnd_perm [] = return []
rnd_perm [x] = return [x]
rnd_perm xs = do
  i <- randomRIO (0, length xs - 1)
  rest <- rnd_perm $ (take i xs) ++ (drop (i+1) xs)
  return $ (xs !! i) : rest
