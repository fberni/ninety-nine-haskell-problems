-- Problem 41
-- λ> goldbachList 9 20
-- [(3,7),(5,7),(3,11),(3,13),(5,13),(3,17)]
-- λ> goldbachList' 4 2000 50
-- [(73,919),(61,1321),(67,1789),(61,1867)]

module P41 where

import P40

goldbachList :: Int -> Int -> [(Int,Int)]
goldbachList m n = map goldbach $ filter even [m..n]

goldbachList' :: Int -> Int -> Int -> [(Int,Int)]
goldbachList' m n k = filter ((k<) . fst) $ map goldbach $ filter even [m..n]
