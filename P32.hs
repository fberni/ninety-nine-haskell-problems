-- Problem 32
-- λ> [myGCD 36 63, myGCD (-3) (-6), myGCD (-3) 6]
-- [9,3,3]

module P32 where

myGCD :: Int -> Int -> Int
myGCD a b
  | b == 0 = a
  | a < b = myGCD b a
  | otherwise = myGCD (a `mod` b) b

