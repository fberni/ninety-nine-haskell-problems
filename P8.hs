-- Problem 8

-- λ> compress "aaaabccaadeeee"
-- "abcade"

module P8 where

compress :: Eq a => [a] -> [a]
compress [] = []
compress all@(x:xs) = x : (map snd $ filter (uncurry (/=)) $ zip all xs)
