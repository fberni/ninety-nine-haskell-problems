-- Problem 20
--λ> removeAt 2 "abcd"
-- ('b',"acd")

module P20 where

removeAt :: Int -> [a] -> (a, [a])
removeAt n xs = (last l, (init l) ++ r)
  where
    xs' = splitAt n xs
    r = snd xs'
    l = fst xs'
