-- Problem 15

-- λ> repli "abc" 3
-- "aaabbbccc"

module P15 where

repli :: [a] -> Int -> [a]
repli [] n = []
repli (x:xs) n = replicate n x ++ repli xs n
