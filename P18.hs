-- Problem 18
--
-- λ> slice ['a','b','c','d','e','f','g','h','i','k'] 3 7
-- "cdefg"


module P18 where

slice :: [a] -> Int -> Int -> [a]
slice xs i j = take (j-i+1) $ drop (i-1) xs

