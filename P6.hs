-- Problem 6

-- λ> isPalindrome [1,2,3]
-- Falsa
-- λ> isPalindrome "madamimadam"
-- True
-- λ> isPalindrome [1,2,4,8,16,8,4,2,1]
-- True

isPalindrome :: Eq a => [a] -> Bool
isPalindrome xs = xs == reverse xs
