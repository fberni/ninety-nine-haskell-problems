-- Problem 39
-- λ> primesR 10 20
-- [11,13,17,19]

primesR :: Int -> Int -> [Int]
primesR m n = takeWhile (<=n) $ dropWhile (<=m) primes
  where primes = sieve (2:[3,5..])
        sieve (p:ps) = p : (sieve [x | x <- ps, x `mod` p /= 0])
