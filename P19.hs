-- Problem 19
--
-- λ> rotate ['a','b','c','d','e','f','g','h'] 3
-- "defghabc"
--
-- λ> rotate ['a','b','c','d','e','f','g','h'] (-2)
-- "ghabcdef"


module P19 where

rotate :: [a] -> Int -> [a]
rotate xs n = uncurry (flip (++)) $ splitAt (n `mod` (length xs)) xs
