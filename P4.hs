-- Problem 4
-- λ> myLength [123, 456, 789]
-- 3
-- λ> myLength "Hello, world!"
-- 13

myLength :: Foldable t => t a -> Int
myLength xs = foldl (\a _ -> a+1) 0 xs
