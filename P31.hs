-- Problem 31
-- λ> isPrime 7
-- True

module P31 where

isPrime :: Int -> Bool
isPrime n
  | n <= 1 = False
  | n == 2 = True
  | otherwise = all (0 /=) $ zipWith mod (repeat n) [2..(n-1)]

