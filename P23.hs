-- Problem 23
-- λ> rnd_select "abcdefgh" 3 >>= putStrLn
-- eda

module P23 where

import Control.Monad
import System.Random

rnd_select :: [a] -> Int -> IO [a]
rnd_select xs n = do
  indices <- replicateM n $ getStdRandom $ randomR (0, (length xs) - 1)
  return [xs !! i | i <- indices]
