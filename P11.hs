-- Problem 11

-- λ> encodeModified "aaaabccaadeeee"
-- [Multiple 4 'a',Single 'b',Multiple 2 'c',
-- Multiple 2 'a',Single 'd',Multiple 4 'e']


module P11 where

import P10

data EncodedItem a = Single a | Multiple Int a
  deriving Show

encodeModified :: Eq a => [a] -> [EncodedItem a]
encodeModified = map helper . encode
  where
    helper (1, x) = Single x
    helper (n, x) = Multiple n x
