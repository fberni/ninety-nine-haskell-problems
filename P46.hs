-- Problem 46

module P46 where

import Control.Monad

and' :: Bool -> Bool -> Bool
and' = (&&)

or' :: Bool -> Bool -> Bool
or' = (||)

nand' :: Bool -> Bool -> Bool
nand' = (not .) . and'

nor' :: Bool -> Bool -> Bool
nor' = (not .) . or'

xor' :: Bool -> Bool -> Bool
xor' a b = a /= b

impl' :: Bool -> Bool -> Bool
impl' True True = True
impl' False _   = True
impl' _ _       = False

equ' :: Bool -> Bool -> Bool
equ' = (not .) . xor'

table :: (Bool -> Bool -> Bool) -> IO ()
table f = mapM_ putStrLn $ map fmt $ liftM2 (,) [True,False] [True,False]
  where
    fmt = (\(a,b) -> show a ++ " " ++ show b ++ " " ++ (show $ f a b))
