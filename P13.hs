-- Problem 13

-- λ> encodeDirect "aaaabccaadeeee"
-- [Multiple 4 'a',Single 'b',Multiple 2 'c',
-- Multiple 2 'a',Single 'd',Multiple 4 'e']

module P13 where

import P11 (EncodedItem(..))

encode :: Eq a => [a] -> [(Int,a)]
encode = foldr helper []
  where
    helper x [] = [(1,x)]
    helper x all@((n,y):xs)
      | x == y = (n+1,y) : xs
      | otherwise = (1,x) : all

encodeDirect :: Eq a => [a] -> [EncodedItem a]
encodeDirect = map helper . encode
  where
    helper (1,x) = Single x
    helper (n,x) = Multiple n x
