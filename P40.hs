-- Problem 40
-- λ> goldbach 28
-- (5, 23)

module P40 where

import Control.Arrow
import Data.List
import Data.Maybe

import P31

goldbach :: Int -> (Int,Int)
goldbach n
  | n `mod` 2 == 0 = fromJust $ find (isPrime . snd) $ map (id &&& (n-)) primes
    where primes = sieve (2:[3,5..])
          sieve (p:ps) = p : (sieve [x | x <- ps, x `mod` p /= 0])
