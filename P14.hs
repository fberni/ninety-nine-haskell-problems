-- Problem 14

-- λ> dupli [1, 2, 3]
-- [1,1,2,2,3,3]

module P14 where

dupli :: [a] -> [a]
dupli = concatMap (replicate 2)
