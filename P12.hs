-- Problem 12

-- λ> decodeModified
--        [Multiple 4 'a',Single 'b',Multiple 2 'c',
--         Multiple 2 'a',Single 'd',Multiple 4 'e']
-- "aaaabccaadeeee"

module P12 where

import P11

decodeModified :: [EncodedItem a] -> [a]
decodeModified = concatMap helper
  where
    helper (Single x) = [x]
    helper (Multiple n x) = replicate n x
