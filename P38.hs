module P38 where

import qualified P34
import qualified P37

totient1 = P34.totient

totient2 = P37.totient
