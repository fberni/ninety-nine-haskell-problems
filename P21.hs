-- Problem 21
-- λ> insertAt 'X' "abcd" 2
-- "aXbcd"

module P21 where

import Control.Arrow

insertAt :: a -> [a] -> Int -> [a]
insertAt x xs n = take (n-1) xs ++ [x] ++ (drop (n-1)) xs
