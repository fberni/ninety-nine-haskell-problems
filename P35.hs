-- Problem 35
-- λ> primeFactors 315
-- [3, 3, 5, 7]

module P35 where

primeFactors :: Int -> [Int]
primeFactors n = helper n 2
  where
    helper 1 _ = []
    helper n i
      | i*i > n = [n]
      | n `mod` i == 0 = i : (helper (n `div` i) i)
      | otherwise = helper n (i+1)
