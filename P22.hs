-- Problem 22
-- λ> range 4 9
-- [4,5,6,7,8,9]


module P22 where

range :: Int -> Int -> [Int]
range m n = [m..n]
