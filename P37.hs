-- Problem 37

module P37 where

import Control.Arrow
import Data.List

import P36

totient :: Int -> Int
totient n = product $ map (\(p, m) -> (p-1) * p ^ (m-1)) $ prime_factors_mult n
