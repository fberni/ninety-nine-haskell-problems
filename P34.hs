-- Problem 34
--

module P34 where

import P33

totient :: Int -> Int
totient n = length $ filter (coprime n) [1..n-1]
