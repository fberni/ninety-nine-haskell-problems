-- Problem 26
-- λ> combinations 3 "abcdef"
-- ["abc","abd","abe",...]

module P26 where

import Data.List

combinations :: Int -> [a] -> [[a]]
combinations 0 xs = [[]]
combinations k [] = []
combinations k l@(x:xs)
  | k > 0 = (map (x :) (combinations (k-1) xs)) ++ (combinations k xs)
  | otherwise = error "k cannot be negative"
