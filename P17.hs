-- Problem 17

-- λ> split "abcdefghik" 3
-- ("abc", "defghik")

module P17 where

import Control.Arrow

split :: [a] -> Int -> ([a],[a])
split xs n = helper ([],xs) n
  where
    helper x 0 = x
    helper l@(xs,[]) n = l
    helper (xs,y:ys) n = helper (xs++[y],ys) (n-1)

