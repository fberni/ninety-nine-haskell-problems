-- Problem 3
-- λ> elementAt [1,2,3] 2
-- 2
-- λ> elementAt "haskell" 5
-- 'e'

module P3 where

elementAt :: [a] -> Int -> a
elementAt (x:_)  1 = x
elementAt []     _ = error "Index out of bounds"
elementAt (_:xs) n
  | n < 1         = error "Index out of bounds"
  | otherwise     = elementAt xs (n-1)
