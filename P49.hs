-- Problem 49

import Control.Applicative

gray :: Int -> [String]
gray 1 = ["0", "1"]
gray n = liftA2 (++) (map ('0' :)) (map ('1' :) . reverse) $ gray $ n-1

main :: IO ()
main = mapM_ putStrLn $ gray 3
