-- Problem 1

-- Find the last element of a list.
-- λ> myLast [1,2,3,4]
-- 4
-- λ> myLast ['x','y','z']
-- 'z'

module P1 where

myLast :: [a] -> a
myLast []     = error "Cannot get the of empty list"
myLast (x:[]) = x
myLast (_:xs) = myLast xs
