-- Problem 24
-- λ> diff_select 6 49
-- [23,1,17,33,21,37]

module P24 where

import Control.Monad
import System.Random

diff_select :: Int -> Int -> IO [Int]
diff_select n m = helper n [1..m]
  where
    helper 0 xs = return []
    helper n xs = do
      i <- getStdRandom $ randomR (0, length xs - 1)
      is <- helper (n-1) (take i xs ++ (drop (i+1) xs))
      return $ (xs !! i) : is
