-- Problem 47

module P47 where

import Control.Monad

and' :: Bool -> Bool -> Bool
and' = (&&)

or' :: Bool -> Bool -> Bool
or' = (||)

nand' :: Bool -> Bool -> Bool
nand' = (not .) . and'

nor' :: Bool -> Bool -> Bool
nor' = (not .) . or'

xor' :: Bool -> Bool -> Bool
xor' a b = a /= b

impl' :: Bool -> Bool -> Bool
impl' True True = True
impl' False _   = True
impl' _ _       = False

equ' :: Bool -> Bool -> Bool
equ' = (not .) . xor'

infixl 6 `and'`
infixl 6 `nand'`
infixl 5 `xor'`
infixl 5 `equ'`
infixl 4 `or'`
infixl 4 `nor'`
infixl 3 `impl'`

table :: (Bool -> Bool -> Bool) -> IO ()
table f = mapM_ putStrLn $ map fmt $ liftM2 (,) [True,False] [True,False]
  where
    fmt = (\(a,b) -> show a ++ " " ++ show b ++ " " ++ (show $ f a b))
