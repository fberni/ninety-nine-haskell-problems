-- Problem 9

-- λ> pack ['a', 'a', 'a', 'a', 'b', 'c', 'c', 'a',
--          'a', 'd', 'e', 'e', 'e', 'e']
-- ["aaaa","b","cc","aa","d","eeee"]

module P9 where

pack :: Eq a => [a] -> [[a]]
pack [] = []
pack (x:xs) = (x : takeWhile (x==) xs) : (pack $ dropWhile (x==) xs)
