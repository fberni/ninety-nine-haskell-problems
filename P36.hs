-- Problem 36
-- λ> prime_factors_mult 315
-- [(3,2),(5,1),(7,1)]

module P36 where

import Control.Arrow
import Data.List

import P35

prime_factors_mult :: Int -> [(Int,Int)]
prime_factors_mult n = map (head &&& length) $ group $ primeFactors n
