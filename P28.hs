-- Problem 28
-- Part 1
-- λ> lsort ["abc","de","fgh","de","ijkl","mn","o"]
-- ["o","de","de","mn","abc","fgh","ijkl"]

-- Part 2
-- λ> lfsort ["abc", "de", "fgh", "de", "ijkl", "mn", "o"]
-- ["ijkl","o","abc","fgh","de","de","mn"]

module P28 where

import Data.List
import Data.Ord
import qualified Data.Map as Map

lsort :: [[a]] -> [[a]]
lsort = sortBy $ comparing length

-- A little convoluted but it works
lfsort :: [[a]] -> [[a]]
lfsort xs =
  let lengths = foldr (\k -> (Map.insertWith (+)) k 1) Map.empty (map length xs)
    in sortBy (comparing ((lengths Map.!) . length)) xs

