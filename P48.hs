-- Problem 48

module P48 where

import P47

import Control.Monad

args :: Int -> [a] -> [[a]]
args 0 xs = return []
args n xs = do ys <- args (n-1) xs
               x <- xs
               return $ ys ++ [x]

tablen :: Int -> ([Bool] -> Bool) -> IO ()
tablen n f = mapM_ putStrLn $ map fmt $ args n [True,False]
  where
    fmt l = foldr (\s a -> show s ++ " " ++ a) "" l ++ (show $ f l)
