-- Problem 33
-- λ> coprime 35 64
-- True

module P33 where

coprime :: Int -> Int -> Bool
coprime a b = a `gcd` b == 1

