-- Problem 16

-- λ> dropEvery "abcdefghik" 3
-- "abdeghk"

module P16 where

dropEvery :: [a] -> Int -> [a]
dropEvery xs n = map fst $ filter ((/=0) . (`mod` n) . snd) $ zip xs [1..]

dropEvery' :: [a] -> Int -> [a]
dropEvery' xs n = dropHelper xs n
  where
    dropHelper [] _ = []
    dropHelper (x:xs) 1 = dropHelper xs n
    dropHelper (x:xs) m = x : dropHelper xs (m-1)
