-- Problem 27
-- λ> group [2,3,4] ["aldo","beat","carla","david","evi","flip","gary","hugo","ida"]
-- [[["aldo","beat"],["carla","david","evi"],["flip","gary","hugo","ida"]],...]
-- (altogether 1260 solutions)
--
-- λ> group [2,2,5] ["aldo","beat","carla","david","evi","flip","gary","hugo","ida"]
-- [[["aldo","beat"],["carla","david"],["evi","flip","gary","hugo","ida"]],...]
-- (altogether 756 solutions)

module P27 where

import P26

group :: Eq a => [Int] -> [a] -> [[[a]]]
group [] xs = [[]]
group (g:gs) xs = do
  first <- combinations g xs
  last <- group gs $ remove first xs
  return $ first:last
  where
    remove ys xs = [x | x <- xs, not $ x `elem` ys]
